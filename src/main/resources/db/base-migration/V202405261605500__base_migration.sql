CREATE TABLE employees (
                           id SERIAL PRIMARY KEY,
                           start_date DATE,
                           cpf VARCHAR(20),
                           job VARCHAR(255),
                           name VARCHAR(255),
                           salary NUMERIC(30, 2),
                           manager_id INTEGER,
                           CONSTRAINT fk_manager FOREIGN KEY (manager_id) REFERENCES employees(id)
);


INSERT INTO employees (start_date, cpf, job, name, salary, manager_id)
VALUES ('2023-04-01', '987.654.321-00', 'SALES', 'Maria Oliveira', 9500.00, NULL);


CREATE TABLE employees_subordinates (
                                        employee_id INTEGER,
                                        underEmployee_id INTEGER,
                                        CONSTRAINT fk_employee FOREIGN KEY (employee_id) REFERENCES employees(id),
                                        CONSTRAINT fk_subordinate FOREIGN KEY (underEmployee_id) REFERENCES employees(id)
);
