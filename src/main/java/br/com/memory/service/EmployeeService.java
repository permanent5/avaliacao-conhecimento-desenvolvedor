package br.com.memory.service;

import br.com.memory.controller.requests.EmployeeUpdateRequest;
import br.com.memory.entity.Employee;
import br.com.memory.exception.BadRequestException;
import br.com.memory.exception.NotFoundException;
import br.com.memory.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository repository;

    public Page<Employee> getAllEmployees(int page, int size){
        return repository.findAll(PageRequest.of(page, size));
    }

    public Employee getById(Long id){
        return repository.findById(id).orElseThrow(() -> new NotFoundException("Usuário não encontrado"));
    }

    public Page<Employee> getAllUnderEmployeesById(Long id, int page, int size){
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAllByManagerId(id, pageable);}

    public Page<Employee> getAllEmployeesByYear(String year, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size);
        return repository.findAllByEntryYear(Integer.parseInt(year), pageRequest);
    }
    public Employee getUpperManager(Long employee1Id, Long employee2Id){
        Employee person1 = repository.findById(employee1Id).orElseThrow(NotFoundException::new);
        Employee person2 = repository.findById(employee2Id).orElseThrow(NotFoundException::new);

        if(person1.getManager() == null || person2.getManager() == null){
            throw new BadRequestException("Um dos dois funcionários não tem um gerente");
        }

        List<String> managerNames = new ArrayList<>();
        Employee manager1 = person1.getManager();

        while (manager1 != null) {
            managerNames.add(manager1.getName());
            manager1 = manager1.getManager();
        }

        while (person2 != null) {
            if (managerNames.contains(person2.getName())) {
                return repository.findById(person2.getId()).orElseThrow(NotFoundException::new);
            }
            person2 = person2.getManager();
        }
        throw new NotFoundException();
    }
    public Employee createEmployee(Employee employee) throws BadRequestException{
        Employee employeeValidated = validateEmployeeData(employee);

        return repository.save(employeeValidated);
    }

    public Employee updateEmployee(Long id, EmployeeUpdateRequest request){
        Employee employee = repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Funcionário não encontrado"));

        employee.setJob(request.getJob());
        employee.setSalary(request.getSalary());

        updateManager(request, employee);

        return repository.save(employee);
    }

    private void updateManager(EmployeeUpdateRequest request, Employee employee) {
        if (request.getManager().getId() != null) {
            if (employee.getManager() == null || !employee.getManager().getId().equals(request.getManager().getId())) {
                Employee newManager = repository.findById(request.getManager().getId())
                        .orElseThrow(() -> new NotFoundException("Novo gerente não encontrado"));
                employee.setManager(newManager);
            }
        }
    }

    public HttpStatus deleteEmployee(Long id) {
        Employee employee = repository.findById(id).orElseThrow(() -> new NotFoundException("Funcionário não encontrado"));
        administrateUnderEmployees(employee);

        repository.deleteById(id);
        return HttpStatus.OK;
    }

    private void administrateUnderEmployees(Employee employee) {
        Employee manager = employee.getManager();
        List<Employee> subordinates = employee.getUnderEmployees();

        Employee newManager = getColleagues(manager, employee);

        newManagerForSubordinates(subordinates, newManager != null ? newManager : manager);
    }

    private void newManagerForSubordinates(List<Employee> subordinates, Employee newManager){
        for(Employee subordinate: subordinates){
            subordinate.setManager(newManager); // newManager pode ser null
            repository.save(subordinate);
        }
    }


    private Employee validateEmployeeData(Employee employee) throws BadRequestException{
        repository.findById(employee.getManager().getId()).orElseThrow(() -> new NotFoundException("O gerente não existe"));

        boolean cpfExists = repository.existsByCpf(employee.getCpf());
        if (cpfExists) throw new BadRequestException("CPF já cadastrado");

        employee.setStartDateStringConverter(employee.getStartDateString());
        return employee;
    }

    private Employee getColleagues(Employee manager, Employee employeeExcluded){

        if (manager != null) {
            List<Employee> colleagues = manager.getUnderEmployees().stream()
                    .filter(underEmployees -> !underEmployees.getCpf().equals(employeeExcluded.getCpf())).toList();
            if (!colleagues.isEmpty()) {
                return colleagues.get(0);
            }
        }
        return manager;
    }
}
