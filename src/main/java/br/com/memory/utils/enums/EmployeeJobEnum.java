package br.com.memory.utils.enums;

public enum EmployeeJobEnum {
    DEVELOPER,
    COMMERCIAL,
    SALES,
    MANAGEMENT
}
