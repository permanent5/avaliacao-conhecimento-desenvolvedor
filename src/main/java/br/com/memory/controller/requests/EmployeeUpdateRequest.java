package br.com.memory.controller.requests;

import br.com.memory.entity.Employee;
import br.com.memory.utils.enums.EmployeeJobEnum;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class EmployeeUpdateRequest {
    @NotNull
    private EmployeeJobEnum job;

    @DecimalMin(value = "1420.00", inclusive = false, message = "Salário deve ser acíma do salário mínimo nacional")
    private BigDecimal salary;

    private Employee manager;
}
