package br.com.memory.controller;

import br.com.memory.controller.requests.EmployeeUpdateRequest;
import br.com.memory.entity.Employee;
import br.com.memory.exception.BadRequestException;
import br.com.memory.exception.NotFoundException;
import br.com.memory.service.EmployeeService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employees")
@RequiredArgsConstructor
public class EmployeeController {
    private final EmployeeService service;

    @GetMapping
    public Page<Employee> getAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size){
        return service.getAllEmployees(page, size);
    }

    @GetMapping("/underemployees/{id}")
    public Page<Employee> getUnderEmployees(@PathVariable Long id, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size){
        return service.getAllUnderEmployeesById(id, page, size);
    }

    @GetMapping("/{id}")
    public Employee getById(@PathVariable Long id) throws NotFoundException {
        return service.getById(id);
    }

    @GetMapping("/year/{year}")
    public Page<Employee> getAllByYear(@PathVariable String year, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size){
        return service.getAllEmployeesByYear(year, page, size);
    }

    @GetMapping("/uppermanager/{employee1}/{employee2}")
    public Employee getUpperManager(@PathVariable Long employee1, @PathVariable Long employee2){
        return service.getUpperManager(employee1, employee2);
    }

    @PostMapping
    public Employee create(@RequestBody @Valid Employee employee) throws BadRequestException {
        return service.createEmployee(employee);
    }

    @PutMapping("/{id}")
    public Employee update(@PathVariable Long id, @RequestBody @Valid EmployeeUpdateRequest employeeUpdateRequest){
        return service.updateEmployee(id, employeeUpdateRequest);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id){
        service.deleteEmployee(id);
        return ResponseEntity.ok("Deletado com sucesso");
    }
}
