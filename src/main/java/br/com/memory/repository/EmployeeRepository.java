package br.com.memory.repository;

import br.com.memory.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    boolean existsByCpf(String cpf);
    boolean existsById(Long id);
    @Query("SELECT e FROM Employee e WHERE YEAR(e.startDate) = :year")
    Page<Employee> findAllByEntryYear(@Param("year") int year, Pageable pageable);

    @Query("SELECT e FROM Employee e WHERE e.manager.id = :managerId")
    Page<Employee> findAllByManagerId(Long managerId, Pageable pageable);}
