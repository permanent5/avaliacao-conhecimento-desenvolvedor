package br.com.memory.entity;

import br.com.memory.utils.enums.EmployeeJobEnum;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.*;
import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.data.annotation.ReadOnlyProperty;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Data
@Entity
@Table(name = "employees")
public class Employee {
    @Id
    @ReadOnlyProperty
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "o nome não pode ser vazio")
    private String name;

    @NotNull
    @CPF
    @Column(name = "cpf", nullable = false, unique = true)
    private String cpf;

    @Transient
    @Pattern(regexp = "^\\d{2}/\\d{2}/\\d{4}$", message = "a data de início deve seguir o formato: dd/MM/yyyy")
    private String startDateString;

    private LocalDate startDate;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Função não pode ser nulo")
    private EmployeeJobEnum job;

    @Column(precision = 30, scale = 2)
    @DecimalMin(value = "1420.00", inclusive = false, message = "Salário deve ser acíma do salário mínimo nacional")
    private BigDecimal salary;

    @OneToMany(mappedBy = "manager")
    private List<Employee> underEmployees;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "manager_id")
    private Employee manager;

    public void setStartDateStringConverter(String startDateString) {
        this.startDateString = startDateString;
        if (startDateString != null && !startDateString.isEmpty()) {
            this.startDate = LocalDate.parse(startDateString, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        }
    }

    public List<Employee> getUnderEmployees() {
        return underEmployees;
    }
}
